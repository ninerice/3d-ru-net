#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 16 21:27:54 2019

@author: customer
"""
import math
import os
import numpy as np
import random
import SimpleITK as sitk
from skimage.measure import label,regionprops
from skimage import filters
from scipy.ndimage.interpolation import shift, zoom
from scipy.ndimage import rotate
import torch
from torch.utils import data
import copy

#Maximum Bbox Cropping to Reduce Image Dimension
def MaxBodyBox(input):
    Otsu=filters.threshold_otsu(input[input.shape[0]//2])
    Seg=np.zeros(input.shape)
    Seg[input>=Otsu]=255
    Seg=Seg.astype(np.int)
    ConnectMap=label(Seg, connectivity= 2)
    Props = regionprops(ConnectMap)
    Area=np.zeros([len(Props)])
    Area=[]
    Bbox=[]
    for j in range(len(Props)):
        Area.append(Props[j]['area'])
        Bbox.append(Props[j]['bbox'])
    Area=np.array(Area)
    Bbox=np.array(Bbox)
    argsort=np.argsort(Area)
    Area=Area[argsort]
    Bbox=Bbox[argsort]
    Area=Area[::-1]
    Bbox=Bbox[::-1,:]
    MaximumBbox=Bbox[0]
    return Otsu,MaximumBbox

def DataLoader(pname, image_path, opt, mode, augment):
    def load_file(name):
        n = os.path.join(image_path, name)
        if not os.path.isfile(n):
            return None
        l = sitk.ReadImage(n)
        return sitk.GetArrayFromImage(l)

    def load_file_MM(t2, t1, t1c):
        p2 = load_file(t2)
        p1 = load_file(t1)
        p1c = load_file(t1c)
        if p1 is None or p1c is None:
            r = np.stack((p2, p2, p2), axis=0)
        else:
            r = np.stack((p2, p1, p1c), axis=0)
        return r

    assert mode in ['Train','Valid','Test'], "{}".format(mode)
    #Image Loading
    if opt.MM == 1:
        ImageInput = load_file('Image_2.mhd')
    else:
        ImageInput = load_file_MM('Image_2.mhd', 'Image_2_T1.mhd', 'Image_2_T1C.mhd')
    RegionLabel = load_file('Label.mhd')
    ContourLabel = load_file('Contour.mhd')

    ImageInput = ImageInput.astype(np.float)
    m = np.amax(ImageInput)
    ImageInput = ImageInput / (m / 255.0)

    #Orig Shape Backup
    Shape=ImageInput.shape
    LShape=RegionLabel.shape

    do_mmbbox = False
    if do_mmbbox:
        #Body Bbox Compute
        Otsu,MaximumBbox=MaxBodyBox(ImageInput)
           
        #Apply BodyBbox Cropping
        # print("maximumBbox", MaximumBbox)
        ImageInput=ImageInput[MaximumBbox[0]:MaximumBbox[3],MaximumBbox[1]:MaximumBbox[4],MaximumBbox[2]:MaximumBbox[5]]    
        RegionLabel=RegionLabel[MaximumBbox[0]:MaximumBbox[3],MaximumBbox[1]:MaximumBbox[4],MaximumBbox[2]:MaximumBbox[5]]
        ContourLabel=ContourLabel[MaximumBbox[0]:MaximumBbox[3],MaximumBbox[1]:MaximumBbox[4],MaximumBbox[2]:MaximumBbox[5]]
    else:
        MaximumBbox = [0, 0, 0] + list(Shape)[-3:]

    if 0:
        for i in (ImageInput, RegionLabel, ContourLabel):
            print("file {}: shapr={} max={} non-zeor={} sum={} avg={}".format(image_path, i.shape, np.amax(i),
                                                                              np.count_nonzero(i), np.sum(i),
                                                                              np.sum(i) / np.count_nonzero(i)))
    ImageInput=ImageInput/255

    if mode =='Train':
        tf = ""
        dim = len(LShape)
        idim = len(Shape)
        # Flipping
        if augment.get("flip", None):
            def my_invert(src, m):
                if len(src.shape) == 3:
                    return src[::m[0], ::m[1], ::m[2]].copy()
                else:
                    return src[::, ::m[0], ::m[1], ::m[2]].copy()
            tlist = [1] * dim
            for i in range(dim):
                tlist[i] = random.randint(0,1) * 2 - 1
            ImageInput = my_invert(ImageInput, tlist)
            RegionLabel = my_invert(RegionLabel, tlist)
            ContourLabel = my_invert(ContourLabel, tlist)
            tf += "invert={} ".format(tlist)

        # Shift
        if augment.get("shift", None):
            def my_shift(src, m):
                #print("my_shift", src.shape, m)
                m = copy.copy(m)
                while len(src.shape) > len(m):
                    m = [0] + m
                return shift(src, shift=m, cval=0)
            tlist = [0] * dim
            for i in range(dim):
                degree = augment.get("shift")
                s = math.floor(Shape[i] * degree)
                tlist[i] = random.randint(-s, s)
            ImageInput = my_shift(ImageInput, tlist)
            RegionLabel = my_shift(RegionLabel, tlist)
            ContourLabel = my_shift(ContourLabel, tlist)
            tf += "shift={} ".format(tlist)

        # rotate
        if augment.get("rotate", None):
            def my_rotate(src, n, m):
                if n == 3:
                    return src
                tup = [(1, 2), (2, 0), (0, 1), None]
                axes = tup[n]
                return rotate(src, axes=axes, angle=m, reshape=False, cval=0.0)
            rotate_ax = random.randint(0, 3)
            degree = augment.get("rotate")
            rotate_degree = random.uniform(-degree, degree)
            ImageInput = my_rotate(ImageInput, rotate_ax, rotate_degree)
            RegionLabel = my_rotate(RegionLabel, rotate_ax, rotate_degree)
            ContourLabel = my_rotate(ContourLabel, rotate_ax, rotate_degree)
            tf += "rotate={}/{:.2f} ".format(rotate_ax, rotate_degree)

        # Zoom
        if augment.get("zoom", None):
            def my_zoom(src, m):
                return zoom(src, zoom=m, cval=0)
            tlist = [0] * dim
            degree = augment.get("zoom")
            if (degree < 1):
                degree = 1 / degree
            for i in range(dim):
                tlist[i] = random.uniform(1 / degree, degree)
            ImageInput = my_zoom(ImageInput, tlist)
            RegionLabel = my_zoom(RegionLabel, tlist)
            ContourLabel = my_zoom(ContourLabel, tlist)
            tf += "zoom={} ".format(tlist)

        # Intensity Scale
        if augment.get("intensity-scale", None):
            degree = augment.get("intensity-scale")
            if (degree < 1):
                degree = 1 / degree
            IntensityScale=random.uniform(1 / degree, degree)
            ImageInput=((ImageInput-0.5)*IntensityScale+0.5)
            tf += "intensity-scale={:.2f} ".format(IntensityScale)

        # Intensity Shift
        if augment.get("intensity-shift", None):
            degree = augment.get("intensity-shift")
            IntensityShift=random.uniform(-degree, degree)
            ImageInput += IntensityShift
            tf += "intensity-shift={:.2f} ".format(IntensityShift)

        # print("    tf: {}".format(tf))

    ImageInput[ImageInput>1]=1
    ImageInput[ImageInput<0]=0
        
    #To Tensor
    if len(ImageInput.shape) == 3:
        ImageTensor=np.zeros([1] + list(ImageInput.shape))
        ImageTensor[0]=ImageInput
    else:
        assert len(ImageInput.shape) == 4
        ImageTensor=ImageInput
    ImageTensor=ImageTensor.astype(np.float)
    ImageTensor=torch.from_numpy(ImageTensor)
    ImageTensor=ImageTensor.float()

    RegionLabelTensor=np.zeros([2,ImageInput.shape[-3],ImageInput.shape[-2],ImageInput.shape[-1]])
    RegionLabelTensor[1]=RegionLabel
    RegionLabelTensor[0]=1-RegionLabel
    RegionLabelTensor=torch.from_numpy(RegionLabelTensor)
    RegionLabelTensor=RegionLabelTensor.float()
    
    ContourLabelTensor=np.zeros([2,ImageInput.shape[-3],ImageInput.shape[-2],ImageInput.shape[-1]])
    ContourLabelTensor[1]=ContourLabel
    ContourLabelTensor[0]=1-ContourLabel
    ContourLabelTensor=torch.from_numpy(ContourLabelTensor)
    ContourLabelTensor=ContourLabelTensor.float()

    return pname, ImageTensor, RegionLabelTensor, ContourLabelTensor, Shape, MaximumBbox

def ArbitraryDataLoader(Patient,opt,Subset='Test'): 
    #Image Loading
    ImageInput=sitk.ReadImage(opt.DATA_ROOT+'/'+Subset+'/'+Patient+'/HighRes/'+'Image_2.mhd')
    ImageInput=sitk.GetArrayFromImage(ImageInput)/255.0
    #Orig Shape Backup
    Shape=ImageInput.shape
    #Body Bbox Compute
    Otsu,MaximumBbox=MaxBodyBox(ImageInput)
           

    #Apply BodyBbox Cropping
    ImageInput=ImageInput[MaximumBbox[0]:MaximumBbox[3],MaximumBbox[1]:MaximumBbox[4],MaximumBbox[2]:MaximumBbox[5]]    
        
    #To Tensor
    ImageTensor=np.zeros([1,1,ImageInput.shape[0],ImageInput.shape[1],ImageInput.shape[2]])
    ImageTensor[0,0]=ImageInput
    ImageTensor=ImageTensor.astype(np.float)
    ImageTensor=torch.from_numpy(ImageTensor)
    ImageTensor=ImageTensor.float()
    ImageTensor = ImageTensor.to(device=opt.GPU)
    
    return ImageTensor,Shape,MaximumBbox

class ImageFolder(data.Dataset):
    def __init__(self, root, mode,
                 augment, opt):
        self.root = root
        self.pnames = [i for i in os.listdir(self.root) if not i.startswith(".")]
        self.image_paths = list(map(lambda x: os.path.join(self.root, x, 'HighRes'), self.pnames))
        self.mode = mode
        self.augment = augment
        self.opt = opt
        print("image count in {} path: {}".format(self.mode, len(self.image_paths)))

    def __getitem__(self, index):
        return DataLoader(self.pnames[index],
                          self.image_paths[index],
                          self.opt,
                          mode=self.mode, augment=self.augment)

    def __len__(self):
        """Returns the total number of font files."""
        return len(self.image_paths)

def get_loader(image_path, mode, opt,
               augment={}, batch_size=1, num_workers=2):
    dataset = ImageFolder(root=image_path,
                          mode=mode,
                          augment=augment,
                          opt=opt)
    data_loader = data.DataLoader(dataset=dataset,
                                  batch_size=batch_size,
                                  shuffle=True,
                                  num_workers=num_workers)
    return data_loader
