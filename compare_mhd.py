#!/usr/bin/env python3
import argparse
import sys

# Connect to the debugging server for interactive debugging outside of the docker that this program runs
# Need to have Eclipse + PyDev extension as the remote debug server. Also use --network="host' in the docker command.
# Consider to include pydev package in the docker image 
# sys.path.append('/home/carolyn/.eclipse/org.eclipse.platform_4.16.0_1473617060_linux_gtk_x86_64/plugins/org.python.pydev.core_7.7.0.202008021154/pysrc')
# import pydevd
# pydevd.settrace("127.0.0.1", port=5678)

#import mayavi
#from mayavi import mlab
import numpy as np
import SimpleITK as sitk
from skimage import measure, feature
import mpl_toolkits
from mpl_toolkits import mplot3d
#from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection

# a useful link
# https://shartoo.github.io/2017/01/20/medical_image_process/

def hmds_to_ndarray(filename):
    l = sitk.ReadImage(filename)
    return sitk.GetArrayFromImage(l)

def get_xyz_mouse_click(event, ax):
    """
    Get coordinates clicked by user
    """
    if ax.M is None:
        return {}

    xd, yd = event.xdata, event.ydata
    p = (xd, yd)
    edges = ax.tunit_edges()
    ldists = [(mpl_toolkits.mplot3d.proj3d._line2d_seg_dist(p0, p1, p), i) for \
                i, (p0, p1) in enumerate(edges)]
    ldists.sort()

    # nearest edge
    edgei = ldists[0][1]

    p0, p1 = edges[edgei]

    # scale the z value to match
    x0, y0, z0 = p0
    x1, y1, z1 = p1
    d0 = np.hypot(x0-xd, y0-yd)
    d1 = np.hypot(x1-xd, y1-yd)
    dt = d0+d1
    z = d1/dt * z0 + d0/dt * z1

    x, y, z = mplot3d.proj3d.inv_transform(xd, yd, z, ax.M)
    return x, y, z

def plot_3d(images, files, use_mesh=False, overlay=False, threshold=0.5, no_3d=False):
    max_slices_number = images[0].shape[0]

    def cal_image_ratio(image1, image2, idx, doPrint):
        c1 = np.count_nonzero(image1)
        c2 = np.count_nonzero(image2)
        ratio = 0 if c1 == 0 else c2/c1
        if doPrint:
            print('{} -- Label: {}, Predict={}, P/L={}'.format("Volume" if idx is None else "Area of Slice #" + str(idx), \
                                                           c1, c2, ratio))

        return ratio

    def cal_image_iou( image1, image2, idx, doPrint, threshold=0.5):
        # Intersection / Union
        image1 = image1 > threshold
        image2 = image2 > threshold #GT == torch.max(GT)

        TP = np.logical_and(image1, image2)
        a = TP.sum()
        b = image1.sum()+image2.sum()-TP.sum()
        if b == 0:
            return 1
        IOU = a/b
        if doPrint:
            print('{} -- Intersection: {}, Union={}, IOU={}'.format("Overall IOU" if idx is None else "IOU of Slice #" + str(idx), \
                                                                    a, b, IOU))
        return IOU
    
    def on_move(event):
        if overlay:
            return
        for a in axs:
            if event.inaxes == a:
                for b in axs:
                    if a != b:
                        b.view_init(elev=a.elev, azim=a.azim)
        figs[0].canvas.draw_idle()

    def show_slice(ax, image, z, cmap):
         z = min(image.shape[2] - 1, int(round(z)))
         z = max(0, z);
         im = image[:,:,z] * 128
         ax.imshow(im, alpha=0.4, cmap=cmap)
         slice_z[0] = z
         ax.title.set_text('slice z={}, Pred/Label={:.3f}, IOU={:.3f}'.format(z, cal_image_ratio(images[0][z], images[1][z], z, False), cal_image_iou(images[0][z], images[1][z], z, False )))
         figs[1].canvas.draw_idle()

    def on_mouse_press(event):
        z = None
        for a in axs:
            if event.inaxes == a:
                x,y,z = get_xyz_mouse_click(event, a)
                print('Clicked at: x={}, y={}, z={}'.format(x, y, z))
        if z is not None and z >= 0:
            show_slices(z)

    def on_key_press(event):
        if (event.key == 'right' or event.key == 'up') and slice_z[0] < max_slices_number-1:
            show_slices(slice_z[0] + 1)
        elif (event.key == 'left' or event.key == 'down') and slice_z[0] > 0:
            show_slices(slice_z[0] - 1)
        elif (event.key == 'escape'):
            sys.exit()
        else:
            print('pressed', event.key)

    def show_slices(z):
        if z is not None and z >= 0 and z < max_slices_number:
            cal_image_ratio(images[0][z], images[1][z], z, True)
            cal_image_iou(images[0][z], images[1][z], z, True)
        
            first_call = True
            for i, image in enumerate(images):
                if overlay:
                    idx = 0
                else:
                    idx = i
                if True:
                    if overlay:
                        if first_call:
                            first_call = False
                            # print("clear window-{}".format(idx))
                            bxs[idx].cla()
                    else:
                        bxs[idx].cla()
                print("show slice#{} of image#{} on window#{}".format(z, i, idx))
                show_slice(bxs[idx], image_npa[i], z, cmap=cmaps[i % len(cmaps)])

    def get_mesh(image, threshold):
        verts,faces = measure.marching_cubes_classic(image, threshold)
        # Fancy indexing: `verts[faces]` to generate a collection of triangles
        mesh = Poly3DCollection(verts[faces], alpha=0.4)
        return mesh

    def get_points(image, threshold):
        x = []
        y = []
        z = []
        for idx, k in np.ndenumerate(image):
            if k > threshold:
                x.append(idx[0])
                y.append(idx[1])
                z.append(idx[2])
        return x, y, z

    if len(images) == 2:
        cal_image_ratio(images[0], images[1], None, True)
        cal_image_iou(images[0], images[1], None, True)

    if (overlay):
        subplot_num = 1
    else:
        subplot_num = len(images)

    figs = [None] * 3;
    figs[0] = plt.figure(figsize=(10 * subplot_num, 10))
    figs[1] = plt.figure(figsize=(10 * subplot_num, 10))
    figs[2] = plt.figure(figsize=(2 * max_slices_number, 2 * subplot_num))
    c1 = figs[0].canvas.mpl_connect('motion_notify_event', on_move)
    c2 = figs[0].canvas.mpl_connect('button_press_event', on_mouse_press)
    c3 = figs[1].canvas.mpl_connect('key_press_event', on_key_press)

    slice_z = [0]
    axs = [None] * subplot_num
    bxs = [None] * subplot_num
    cxs = [None] * subplot_num * max_slices_number
    cmaps=['Reds', 'Greens', 'Blues']
    colors = [[1, 0.5, 0.5], [0.5, 1, 0.5], [0.5, 0.5, 1], [0.1, 0.1, 0.1]]
    image_npa = [None] * len(images)
    for i, image in enumerate(images):
        image_npa[i] = image.transpose(2,1,0)
        if overlay:
            idx = 0
        else:
            idx = i
        if axs[idx] is None:
            if not no_3d:
                axs[idx] = figs[0].add_subplot(1, subplot_num, idx + 1, projection='3d')
        if bxs[idx] is None:
            bxs[idx] = figs[1].add_subplot(1, subplot_num, idx + 1)
        if not overlay and axs[idx]:
            axs[idx].title.set_text(files[i])
        ax = axs[idx]
        face_color = colors[i % len(colors)]
        for j in range(image.shape[0]):
            if j >= max_slices_number:
                continue
            jx = idx * max_slices_number + j
            if cxs[jx] is None:
                cxs[jx] = figs[2].add_subplot(subplot_num, max_slices_number, jx + 1)
            show_slice(cxs[jx], image_npa[i], j, cmap=cmaps[i % len(cmaps)])
        if not no_3d:
            print("drew 3d")
            if use_mesh:
                mesh = get_mesh(image_npa[i], threshold)
                mesh.set_facecolor(face_color)
                ax.add_collection3d(mesh)
            else:
                x,y,z = get_points(image_npa[i], threshold)
                print(len(x))
                ax.scatter(x, y, z, c=face_color, alpha=0.1)
                #mlab.points3d(x, y, z, colormap="copper", scale_factor=.25)

            ax.set_xlim(0, image_npa[i].shape[0])
            ax.set_ylim(0, image_npa[i].shape[1])
            ax.set_zlim(0, image_npa[i].shape[2])
            ax.title.set_text('Pred/Label={:.3f}, IOU={:.3f}'.format(cal_image_ratio(images[0], images[1], None, False),cal_image_iou(images[0], images[1], None, False )))
    show_slices(7)

    plt.show()

def draw_mhds(files, use_mesh, overlay, no_3d):
    arr = []
    for f in files:
        arr.append(hmds_to_ndarray(f))

    plot_3d(arr, files, use_mesh, overlay, 0.5, no_3d)

if __name__ == "__main__":
    print('Argument List:', str(sys.argv))
    parser = argparse.ArgumentParser(description='compare mhd files')
    parser.add_argument('-s', '--source', type=str, nargs='+', required=True);
    parser.add_argument("--overlay", action="store_true")
    parser.add_argument("--no-3d", action="store_true")
    parser.add_argument("--mesh", action="store_true")
    parser.add_argument("--agg", default="", help="valid selection is QT5Agg,QT4Agg")
    args = parser.parse_args()
    if args.agg:
        import matplotlib as mpl
        mpl.use(args.agg)
    import matplotlib.pyplot as plt
    draw_mhds(args.source, args.mesh, args.overlay, args.no_3d)
