#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 11 15:10:18 2018

@author: customer
"""
import argparse
import pathlib
import os
import sys
import time
import datetime
import multiprocessing
import SimpleITK as sitk
import numpy as np
import random
import torch
from torch import optim
import torch.nn.functional as F
from torch.autograd import Variable
import cv2
#from tensorboardX import SummaryWriter
from torch.utils.tensorboard import SummaryWriter

from data_loader import get_loader
from model import RU_Net
from evaluation import *

class StreamTee(object):
    # Based on https://gist.github.com/327585 by Anand Kunal
    def __init__(self, terminal, logfile):
        self.terminal = terminal
        self.logfile = logfile
        self.last_time = time.time()

    def write(self, message):
        self.terminal.write(message)
        self.logfile.write(message)
        t = time.time()
        if t > self.last_time + 1:
            self.flush()
            self.last_time = t

    def flush(self):
        self.terminal.flush()
        self.logfile.flush()


class Config():
    STAGE_DILATIONS={'RF64':[1,1,1],'RF88':[1,1,2],'RF112':[1,2,2]}
    def __init__(self):
        self.DICT_CLASS={0:'Background', 1:'Cancer'}
        self.MAX_ROIS_TEST={'Background':0,'Cancer':10}
        self.MAX_ROIS_TRAIN={'Background':0,'Cancer':2}
        self.MAX_ROI_SIZE=[24,96,96]
        self.TO_SPACING=[1,1,4]
        self.DOWN_SAMPLE=[2,4,4]
        # self.DATA_ROOT='./Data/'
        self.INPLACE=True
        self.GPU='cuda'
        self.BASE_CHANNELS=48
        self.run_state = {"epoch_done" : -1, "pre_epoch_done":  -1}
        self.Model = None
        self.optimizer = None
        self.loaded_optimizer_state = None

    def set_args(self, args, cmdline):
        self.DATA_ROOT = args.data_root
        self.WEIGHT_ROOT = args.weight_root
        self.TEST_ONLY = args.test_only
        self.MAX_STEPS = args.max_steps
        self.MAX_EPOCHS = args.epoch
        if args.alt_epoch:
            self.MAX_EPOCHS = args.alt_epoch
            print("use alt_epoch {}".format(self.MAX_EPOCHS))
        self.PRE_EPOCHS = args.pre_epoch
        self.BASE_CHANNELS = args.base_channels
        self.TAG = args.tag
        self.LR = args.learning_rate
        if args.alt_learning_rate:
            self.LR = args.alt_learning_rate
            print("use alt_lr {}".format(self.LR))
        self.LR_DECAY_SKIP_EPOCHS = args.lr_decay_wait_epoch
        self.LR_DECAY_HALF_EPOCHS = args.lr_decay_half_epoch
        self.STAGE_DILATION=Config.STAGE_DILATIONS[args.dilation]
        self.TF_LOGDIR = args.tf_logdir
        self.DILATION = args.dilation
        self.MM = args.mm
        parent_dir = self.WEIGHT_ROOT
        self.REAL_TAG = "{}-{}-{}-{}-{}-{}.".format(self.TAG,
                                                    args.dilation,
                                                    args.base_channels,
                                                    args.pre_epoch,
                                                    args.epoch,
                                                    args.learning_rate)
        './Weights/'
        if self.TAG == 'none':
            prefix = os.path.join(parent_dir,
                                  "{}.".format(args.dilation))
        else:
            prefix = os.path.join(parent_dir, self.REAL_TAG)

        self.WEIGHT_PATH=prefix + "pkl"
        pathlib.Path(parent_dir).mkdir(parents=True, exist_ok=True)
        self.cmdline_path = prefix + "cmd"
        if not self.TEST_ONLY:
            with open(self.cmdline_path, 'a+') as out_file:
                out_file.write(cmdline + '\n')
            logfile = open(prefix + "log", "a+")
            sys.stdout = StreamTee(sys.stdout, logfile)
            sys.stderr = StreamTee(sys.stderr, logfile)

    def set_lr(self, epoch):
        if epoch < self.LR_DECAY_SKIP_EPOCHS:
            print('Learning rate no change. lr = {}'.format(self.LR))
            lr = self.LR
        else:
            i = epoch - self.LR_DECAY_SKIP_EPOCHS + 1
            lr = self.LR / ( 2.0 ** (i / float(self.LR_DECAY_HALF_EPOCHS)))
            print('Decay learning rate: lr = {}'.format(lr))
        for param_group in self.optimizer.param_groups:
            param_group['lr'] = lr

    def save_state(self):
        state = {
            'model_state': self.Model.state_dict(),
            'optimizer_state': self.optimizer.state_dict(),
            'run_state': self.run_state
        }
        torch.save(state, self.WEIGHT_PATH)
        print('Weights Saved to {}'.format(self.WEIGHT_PATH))

    def set_optimizer(self, o):
        self.optimizer = o
        if o is None:
            return
        if self.loaded_optimizer_state:
            self.optimizer.load_state_dict(self.loaded_optimizer_state)
            self.loaded_optimizer_state = None

    def load_state(self):
        state = torch.load(opt.WEIGHT_PATH, map_location=self.GPU)
        self.Model.load_state_dict(state['model_state'])
        self.loaded_optimizer_state = state['optimizer_state']
        self.run_state = state['run_state']
        print('Weights Loaded from {}!'.format(self.WEIGHT_PATH))

opt=Config()
tf_metrics_label = ["IOU", "Accuracy", "Sensitivity", "Specificity", "Precision", "DICE", "POS%"]

def MultiClassDiceLossFunc(y_pred,y_true):
    overlap=torch.zeros([1]).cuda(opt.GPU)
    bottom=torch.zeros([1]).cuda(opt.GPU)
    for i in range(1,len(opt.DICT_CLASS.keys())):
        overlap+=torch.sum(y_pred[0,i]*y_true[0,i])
        bottom+=torch.sum(y_pred[0,i])+torch.sum(y_true[0,i])
    return 1-2*(overlap+1e-4)/(bottom+1e-4)
def RoIDiceLossFunc(y_pred,y_true):
    overlap=torch.zeros([1]).cuda(opt.GPU)
    bottom=torch.zeros([1]).cuda(opt.GPU)
    for i in range(len(y_pred)):
        for j in range(1,len(opt.DICT_CLASS.keys())):
            overlap+=torch.sum(y_pred[i][0,j]*y_true[i][0,j])
            bottom+=torch.sum(y_pred[i][0,j])+torch.sum(y_true[i][0,j])
    return (1-2*overlap/bottom)

def Predict(pname, Image, LabelRegion, LabelContour,
            Shape, MaximumBbox, Subset, opt, save_mhd):
    assert len(pname) == 1, pname
    if opt.MM != 1:
        Shape = tuple(list(Shape)[1:])
    pname = pname[0]
    Image = Image.to(device=opt.GPU)
    LabelRegion = LabelRegion.to(device=opt.GPU)
    LabelContour = LabelContour.to(device=opt.GPU)
    Label=LabelRegion.to('cpu').detach().numpy()

    with torch.no_grad():
        PredSeg=opt.Model.forward(Image)
    RegionOutput=np.zeros(Label.shape)
    RegionWeight=np.zeros(Label.shape)+0.001
    RoIs=PredSeg[2]
    #Apply RoI region predictions to in-body volume container
    #If overlapped, average
    for i in range(len(PredSeg[0])):
        Coord=RoIs[i]*np.array([2,4,4,2,4,4])
        Weight=np.ones(np.asarray(PredSeg[0][i][0].shape))
        RegionOutput[0,:,Coord[0]:Coord[3],Coord[1]:Coord[4],Coord[2]:Coord[5]]+=PredSeg[0][i][0].to('cpu').detach().numpy()
        RegionWeight[0,:,Coord[0]:Coord[3],Coord[1]:Coord[4],Coord[2]:Coord[5]]+=Weight
    RegionOutput/=RegionWeight
    
    #Apply RoI contour predictions to in-body volume container
    #If overlapped, average
    ContourOutput=np.zeros(Label.shape)
    ContourWeight=np.zeros(Label.shape)+0.001
    RoIs=PredSeg[2]
    for i in range(len(PredSeg[0])):
        Coord=RoIs[i]*np.array([2,4,4,2,4,4])
        Weight=np.ones(np.asarray(PredSeg[0][i][0].shape))
        ContourOutput[0,:,Coord[0]:Coord[3],Coord[1]:Coord[4],Coord[2]:Coord[5]]+=PredSeg[1][i][0].to('cpu').detach().numpy()
        ContourWeight[0,:,Coord[0]:Coord[3],Coord[1]:Coord[4],Coord[2]:Coord[5]]+=Weight
    ContourOutput/=ContourWeight
    
    #Apply in-body volume container to original volume size
    OutputWhole1=np.zeros(Shape,dtype=np.uint8)
    OutputWhole2=np.zeros(Shape,dtype=np.uint8)
    OutputWhole=np.zeros(Shape,dtype=np.uint8)
    print(OutputWhole1.shape, MaximumBbox, RegionOutput.shape)
    OutputWhole1[MaximumBbox[0]:MaximumBbox[3],MaximumBbox[1]:MaximumBbox[4],MaximumBbox[2]:MaximumBbox[5]]=(RegionOutput[0,1]*255).astype(np.uint8)
    OutputWhole2[MaximumBbox[0]:MaximumBbox[3],MaximumBbox[1]:MaximumBbox[4],MaximumBbox[2]:MaximumBbox[5]]=(ContourOutput[0,1]*255).astype(np.uint8)
    #Save binary predictions
    OutputWhole[OutputWhole1>=128]=1
    OutputWhole[OutputWhole1<128]=0
    RegionOutput[RegionOutput>=0.5]=1
    RegionOutput[RegionOutput<0.5]=0
    metrics, metrics_label = get_IOU_acc_sen_spec_pre(RegionOutput[0,1], Label[0,1])

    Loss=1-2*np.sum(RegionOutput[0,1]*Label[0,1])/(np.sum(RegionOutput[0,1])+np.sum(Label[0,1]))
    
    #Draw bounding-boxes
    OutputWholeOrig = OutputWhole.copy()
    for Rid in range(len(RoIs)):
        color=(Rid+1,Rid+1,Rid+1)
        
        Coord=RoIs[Rid]*np.array([2,4,4,2,4,4])+np.array([MaximumBbox[0],MaximumBbox[1],MaximumBbox[2],MaximumBbox[0],MaximumBbox[1],MaximumBbox[2]])
        #Out-of-volume protection
        for protect in range(3):
            if Coord[protect+3]>=OutputWhole.shape[protect+0]:
                Coord[protect+3]=OutputWhole.shape[protect+0]-1
        #Draw rectangles
        Rgb=np.zeros([OutputWhole.shape[1],OutputWhole.shape[2],3],dtype=np.uint8)
        Rgb[:,:,0]=OutputWhole[Coord[0]]
        OutputWhole[Coord[0]]=cv2.rectangle(Rgb,(Coord[2],Coord[1]),(Coord[5],Coord[4]),color=color,thickness=2)[:,:,0]
        Rgb[:,:,0]=OutputWhole[Coord[3]]
        OutputWhole[Coord[3]]=cv2.rectangle(Rgb,(Coord[2],Coord[1]),(Coord[5],Coord[4]),color=color,thickness=2)[:,:,0]
        
        Rgb=np.zeros([OutputWhole.shape[0],OutputWhole.shape[1],3],dtype=np.uint8)
        Rgb[:,:,0]=OutputWhole[:,:,Coord[2]]
        OutputWhole[:,:,Coord[2]]=cv2.rectangle(Rgb,(Coord[1],Coord[0]),(Coord[4],Coord[3]),color=color,thickness=2)[:,:,0]
        Rgb[:,:,0]=OutputWhole[:,:,Coord[5]]
        OutputWhole[:,:,Coord[5]]=cv2.rectangle(Rgb,(Coord[1],Coord[0]),(Coord[4],Coord[3]),color=color,thickness=2)[:,:,0]
        
        Rgb=np.zeros([OutputWhole.shape[0],OutputWhole.shape[2],3],dtype=np.uint8)
        Rgb[:,:,0]=OutputWhole[:,Coord[1],:]
        OutputWhole[:,Coord[1],:]=cv2.rectangle(Rgb,(Coord[2],Coord[0]),(Coord[5],Coord[3]),color=color,thickness=2)[:,:,0]
        Rgb[:,:,0]=OutputWhole[:,Coord[4],:]
        OutputWhole[:,Coord[4],:]=cv2.rectangle(Rgb,(Coord[2],Coord[0]),(Coord[5],Coord[3]),color=color,thickness=2)[:,:,0]

    if save_mhd:
        OutputWholeOrig=sitk.GetImageFromArray(OutputWholeOrig)
        OutputWholeOrig.SetSpacing(opt.TO_SPACING)
        OutputWhole=sitk.GetImageFromArray(OutputWhole)
        OutputWhole.SetSpacing(opt.TO_SPACING)
        OutputWhole1=sitk.GetImageFromArray(OutputWhole1)
        OutputWhole1.SetSpacing(opt.TO_SPACING)
        OutputWhole2=sitk.GetImageFromArray(OutputWhole2)
        OutputWhole2.SetSpacing(opt.TO_SPACING)
        Label=sitk.GetImageFromArray(Label[0][1])
        Label.SetSpacing(opt.TO_SPACING)
        if os.path.exists('./Output/'+ pname)==False:
            os.makedirs('./Output/'+ pname)
        sitk.WriteImage(Label,'./Output/' + pname + '/Label_'+opt.TAG+'.mhd')
        sitk.WriteImage(OutputWholeOrig,'./Output/' + pname + '/Pred_'+opt.TAG+'.mhd')
        sitk.WriteImage(OutputWhole,'./Output/' + pname + '/PredBox_'+opt.TAG+'.mhd')
        sitk.WriteImage(OutputWhole1,'./Output/' + pname + '/PredRegion_'+opt.TAG+'.mhd')
        sitk.WriteImage(OutputWhole2,'./Output/' + pname + '/PredContour'+opt.TAG+'.mhd')
        vol = np.count_nonzero(OutputWholeOrig)
    else:
        vol = 0
    return pname, Loss, vol, len(RoIs), metrics, metrics_label

def WriteToTensorboard(writer, name, loss, vol, metrics, tf_metrics_label, metrics_count, step):
    for i in range(len(metrics)):
        writer.add_scalar(name + "/" + tf_metrics_label[i], metrics[i] / metrics_count, step)  
    if name != "Train":
        writer.add_scalar(name + "/Loss", loss / metrics_count, step)
        writer.add_scalar(name + "/Volume", vol, step)

def ToTensor(input):
    return 0

def curb_step(step, config_max):
    if config_max:
        return min(step, config_max)
    else:
        return step

if __name__=='__main__':
    cmdline = " ".join(sys.argv[:])

    parser = argparse.ArgumentParser()
    parser.add_argument('--data-root', type=str, default='./Data/')
    parser.add_argument('--weight-root', type=str, default='./Weights/')
    parser.add_argument('--tag', required=True, type=str)
    parser.add_argument("--test-only", action="store_true")
    parser.add_argument("--mm", type=int, default=1, help="num of modulars")
    parser.add_argument("--max-steps", type=int, default=0)
    parser.add_argument("--pre-epoch", type=int, default=40)
    parser.add_argument("--epoch", type=int, default=50)
    parser.add_argument("--alt-epoch", type=int, default=0)
    parser.add_argument("--learning-rate", type=float, default=0.0001)
    parser.add_argument("--alt-learning-rate", type=float, default=0)
    parser.add_argument("--aug-flip", type=int, default=1)
    parser.add_argument("--aug-shift", type=float, default=0.10)
    parser.add_argument("--aug-rotate", type=float, default=0)
    parser.add_argument("--aug-zoom", type=float, default=0)
    parser.add_argument("--aug-intensity-scale", type=float, default=1.1)
    parser.add_argument("--aug-intensity-shift", type=float, default=0.1)
    parser.add_argument("--lr-decay-wait-epoch", type=int, default=10)
    parser.add_argument("--lr-decay-half-epoch", type=int, default=20)
    parser.add_argument("--base-channels", type=int, default=48)
    statge_dilation_keys = list(Config.STAGE_DILATIONS.keys())
    parser.add_argument('--dilation', choices=statge_dilation_keys, default=statge_dilation_keys[0])
    parser.add_argument('--tf-logdir', type=str, default='./logs/')
    args = parser.parse_args()

    opt.set_args(args, cmdline)
    print("cmd is {}".format(cmdline))
    start_time_gl = time.time()
    thread_num = multiprocessing.cpu_count()
    print("start_time={} thread_num={}".format(datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"),
                                               thread_num))

    augment = {
        "flip": args.aug_flip,
        "shift": args.aug_shift,
        "rotate": args.aug_rotate,
        "zoom": args.aug_zoom,
        "intensity-scale": args.aug_intensity_scale,
        "intensity-shift": args.aug_intensity_shift,
    }

    start_time_gl = time.time()
    opt.Model=RU_Net(opt)
    opt.Model=opt.Model.to(opt.GPU)

    TestPatient=os.listdir(opt.DATA_ROOT+'/Test')
    NumTest=len(TestPatient)

    summary_tag = opt.TF_LOGDIR + opt.REAL_TAG
    writer = SummaryWriter(summary_tag, flush_secs=120)

    if not opt.TEST_ONLY:
        train_data_loader = get_loader(opt.DATA_ROOT+'/Train', mode='Train', opt=opt,
                                       augment=augment, batch_size=1, num_workers=thread_num)
        validate_data_loader = get_loader(opt.DATA_ROOT+'/Valid', mode='Valid', opt=opt,
                                          augment={}, batch_size=1, num_workers=thread_num)
        print("data_root={}".format(opt.DATA_ROOT))
        try:
            opt.load_state()
        except Exception as e:
            print("No weight found at {}, {}".format(opt.WEIGHT_PATH, e))
            pass

        if True:
            #Train Global Image Encoder and RoI locator
            for epoch in range(opt.PRE_EPOCHS):
                if epoch <= opt.run_state["pre_epoch_done"]:
                    print("skip partial-train epoch {}".format(epoch))
                    continue
                if opt.optimizer is None:
                    opt.set_optimizer(optim.Adam(list(opt.Model.GlobalImageEncoder.parameters()),lr=opt.LR,amsgrad=True))
                opt.Model.train()
                step = 0
                start_time = time.time()
                total_loss = 0;
                for iteration, (_, Image, LabelRegion, LabelContour, _, _) in enumerate(train_data_loader):
                    if 1:
                        Image = Image.to(device=opt.GPU)
                        LabelRegion = LabelRegion.to(device=opt.GPU)
                        LabelContour = LabelContour.to(device=opt.GPU)
                        opt.Model.train()
                        Label=LabelRegion
                        opt.optimizer.zero_grad()
                        PredSeg=opt.Model.forward_RoI_Loc(Image,LabelRegion)#Model.train_forward(Image,LabelRegion,LabelContour,UseRoI=True)
                        LossG=MultiClassDiceLossFunc(PredSeg[0],PredSeg[1])
                        LossAll=LossG
                        LossAll.backward()
                        opt.optimizer.step()
                        LossG=LossG.to('cpu').detach().numpy()
                        total_loss += LossG
                        print('  partial-train-{}-{} loss_g={}'.format(epoch, iteration, LossG))
                        step += 1
                    if opt.MAX_STEPS and step >= opt.MAX_STEPS:
                        print("reach max_step {}, break".format(opt.MAX_STEPS))
                        break

                Loss=[]
                opt.set_lr(epoch)
                opt.run_state["pre_epoch_done"] = epoch
                opt.save_state()
                end_time = time.time()
                print('average-partial-train-{}: time={:.2f}s loss={}'.format(epoch,
                                                                              (end_time - start_time) / step,
                                                                              total_loss/ step))
        opt.set_optimizer(None)
        #torch.cuda.empty_cache()

        #Jointly train Global Image Encoder, RoI locator and Local Region Decoder
        Lowest=1
        opt.set_optimizer(optim.Adam(list(opt.Model.GlobalImageEncoder.parameters())+\
                                     list(opt.Model.LocalRegionDecoder.parameters()),lr=opt.LR,amsgrad=True))
        for epoch in range(opt.MAX_EPOCHS):
            if epoch <= opt.run_state["epoch_done"]:
                print("skip train epoch {}".format(epoch))
                continue
            if opt.optimizer is None:
                opt.set_optimizer(optim.Adam(list(opt.Model.GlobalImageEncoder.parameters())+\
                                             list(opt.Model.LocalRegionDecoder.parameters()),lr=opt.LR,amsgrad=True))
            step = 0
            
            start_time = time.time()
            print('Epoch {}/{}'.format(epoch, opt.MAX_EPOCHS))
            opt.Model.train()#set_training(True)
            M = None
            LossG_sum = 0
            LossR_sum = 0
            LossC_sum = 0
            LossAll_sum = 0
            for iteration,  (_, Image, LabelRegion, LabelContour, _, _) in enumerate(train_data_loader):
                if 1:
                    Image = Image.to(device=opt.GPU)
                    LabelRegion = LabelRegion.to(device=opt.GPU)
                    LabelContour = LabelContour.to(device=opt.GPU)
                    opt.optimizer.zero_grad()
                    PredSeg=opt.Model.TrainForward(Image,LabelRegion,LabelContour)
                    LossG=MultiClassDiceLossFunc(PredSeg[-1][0],PredSeg[-1][1])
                    LossR=RoIDiceLossFunc(PredSeg[0],PredSeg[2])
                    LossC=RoIDiceLossFunc(PredSeg[1],PredSeg[3])
                    CWeight=1.0
                    LossAll=LossG+LossR+CWeight*LossC
                    LossAll.backward()
                    opt.optimizer.step()
                    LossG=LossG.to('cpu').detach().numpy()
                    LossR=LossR.to('cpu').detach().numpy()
                    LossC=LossC.to('cpu').detach().numpy()
                    LossG_sum += LossG
                    LossR_sum += LossR
                    LossC_sum += LossC
                    LossAll=LossG+LossR+CWeight*LossC

                    Label=LabelRegion.to('cpu').detach().numpy()
                    RegionOutput=np.zeros(Label.shape)
                    RegionWeight=np.zeros(Label.shape)+0.001
                    RoIs=PredSeg[4]
                    for i in range(len(PredSeg[0])):
                        Coord = RoIs[i] * np.array([2,4,4,2,4,4])
                        Weight=np.ones(np.asarray(PredSeg[0][i][0].shape))
                        RegionOutput[0,:,Coord[0]:Coord[3],Coord[1]:Coord[4],Coord[2]:Coord[5]]+=PredSeg[0][i][0].to('cpu').detach().numpy()
                        RegionWeight[0,:,Coord[0]:Coord[3],Coord[1]:Coord[4],Coord[2]:Coord[5]]+=Weight
                    RegionOutput/=RegionWeight
                    metrics, metrics_label = get_IOU_acc_sen_spec_pre(RegionOutput[0,1], Label[0,1])
                    if M is None:
                        M = metrics
                    else:
                        M = M + metrics
                        print('  e2e-train-{}-{} loss=[{},{},{},{}] metrics={}'.format(epoch, iteration,
                                                                                       LossAll, LossG, LossR, LossC,
                                                                                       list(metrics)))
                    step += 1
                if opt.MAX_STEPS and step >= opt.MAX_STEPS:
                    print("reach max_step {}, break".format(opt.MAX_STEPS))
                    break
            opt.set_lr(epoch)
            Loss=[]
            opt.Model.eval()#set_training(False)
            LossAll_sum=LossG_sum+LossR_sum+CWeight*LossC_sum
            end_time = time.time()
            print('average-e2e-train-{}: time={:.2f}s loss=[{},{},{},{}] {}={}'.format(epoch,
                                                                                       (end_time - start_time) / step,
                                                                                       LossAll_sum / step,
                                                                                       LossG_sum / step,
                                                                                       LossR_sum / step,
                                                                                       LossC_sum / step,
                                                                                       metrics_label, list(M / step)))
            WriteToTensorboard(writer, "Training", 0, 0, M, tf_metrics_label, step, epoch)

            #Model selection according to Global Dice
            M = None
            start_time = time.time()
            step = 0
            for iteration, (pname, Image, LabelRegion, LabelContour, Shape, mmbox) in enumerate(validate_data_loader):
                if 1:
                    pname, Loss_temp, vol_temp, NumRoIs, metrics, metrics_label = Predict(pname, Image, LabelRegion,
                                                                                LabelContour, Shape, mmbox, 'Valid', opt, False)
                    metrics = np.array(metrics)
                    if M is None:
                        M = metrics
                    else:
                        M = M + metrics
                    Loss+=[Loss_temp]
                    print('  val-{}-{} loss={:.2f} metrics={}'.format(epoch, iteration, Loss_temp, list(metrics)))
                    step += 1
                if opt.MAX_STEPS and step >= opt.MAX_STEPS:
                    print("reach max_step {}, break".format(opt.MAX_STEPS))
                    break
                
            Loss=np.mean(np.array(Loss))
            if True or Loss < Lowest:
                print('Loss is {}, lowest is {}'.format(Loss, Lowest))
                Lowest = Loss
                opt.run_state["epoch_done"] = epoch
                opt.save_state()
            end_time = time.time()
            print('average-val-{}: time={:.2f}s loss={:.2f} metrics={}'.format(epoch,
                                                                               (end_time - start_time) / step,
                                                                               Loss, list(M / step)))
            print('Best Loss=', Lowest)
            WriteToTensorboard(writer, "Validation", Loss, 0, M, tf_metrics_label, step, epoch)
    
    if True:
        test_data_loader = get_loader(opt.DATA_ROOT+'/Test', mode='Test', opt=opt,
                                      augment={}, batch_size=1, num_workers=thread_num)
        opt.load_state()
        opt.Model.eval()
        #Lowest=1
        Loss=0
        NumRoIs=0
        M = None
        step = 0
        start_time = time.time()
        for iteration, (pname, Image, LabelRegion, LabelContour, Shape, mmbox) in enumerate(test_data_loader):
            if 1:
                pname, Loss_temp, vol, NumRoI, metrics, metrics_label = Predict(pname, Image, LabelRegion,
                                                                           LabelContour, Shape, mmbox, 'Test', opt, True)
                metrics = np.array(metrics)
                if M is None:
                    M = metrics
                else:
                    M = M + metrics
                NumRoIs +=NumRoI
                Loss+=Loss_temp
                print('  test-{}-{} loss={:.2f} volume={} metrics={}'.format(iteration, pname, Loss_temp, vol, list(metrics)))
                WriteToTensorboard(writer, "Test", Loss, vol, M, tf_metrics_label, step, iteration)
                step += 1
            if opt.MAX_STEPS and step >= opt.MAX_STEPS:
                print("reach max_step {}, break".format(opt.MAX_STEPS))
                break
        end_time = time.time()
        print('Mean RoI = ',NumRoIs / step)
        Loss/=NumTest
        print('average-test: time={:.2f}s loss={:.2f} {}={}'.format((end_time - start_time) / step,
                                                                    Loss, metrics_label, list(M / step)))
    print("End_time={}, used_time={:.2f}s".format(datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"), time.time() - start_time_gl))

