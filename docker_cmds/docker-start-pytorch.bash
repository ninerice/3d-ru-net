#!/bin/bash
IMG=ninerice/pytorch:pytorch14-gpu-python37-vision
docker pull $IMG && \
nvidia-docker run --runtime=nvidia  --rm -it -l tenx \
   --shm-size 8G \
   -e DISPLAY=$DISPLAY \
   -v /tmp/.X11-unix:/tmp/.X11-unix \
   -u $(id -u ${USER}):$(id -g ${USER}) \
   -w "`pwd`" \
   -v /etc/group:/etc/group \
   -v /etc/passwd:/etc/passwd \
   -v /etc/passwd-:/etc/passwd- \
   -v /etc/shadow:/etc/shadow \
   -v /etc/group-:/etc/group- \
   -v /etc/sudoers:/etc/sudoers \
   -v /home:/home \
   -v /tmp/cache123:/home/${USER}/.cache \
   -v /tmp/:/home/${USER}/.local \
   -p 6006:6006 \
   -p 8888:8888 \
   $IMG \
   bash  --noprofile --norc
