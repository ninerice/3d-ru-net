#!/usr/bin/env python
class option():
    def __init__(self, name, augment, choises):
        self.name = name
        self.augment = augment
        self.choises = choises

options = [option("learning-rate", "--learning-rate", [0.00005, 0.000025]),
           option("pre-epoch", "--pre-epoch", [40]),
           option("epoch", "--epoch", [100, 150]),
           option("lr-decay-wait", "--lr-decay-wait-epoch", [20]),
           option("lr-decay-rate", "--lr-decay-half-epoch", [20, 35]),
           option("aug-zoom", "--aug-zoom", [0, 1.1]),
           option("aug-rotate", "--aug-rotate", [0, 10]),
           option("dilation", "--dilation", ["RF112"]),
]

exec_file = "./train.py"
data_root = "~/processed-data/Data-256-0-1/"

test_cases = [""]
for o in options:
    #print("process", o.name, "iterate", o.choises)
    cases = []
    #print("test_cases before:" , test_cases)
    for c in test_cases:
        for p in o.choises:
            new_case = c + "{} {} ".format(o.augment, p)
            #print("new_case", new_case)
            cases.append(new_case)
    test_cases = cases
    #print("test_cases after:" , test_cases)

for i, c in enumerate(test_cases):
    print("===============")
    print("#case-{}: {} {} --data-root {} --tag find-best-A-{}".format(i, exec_file, c, data_root, i))
    print("owner: (Available)")
    print("result:")
    print("")
